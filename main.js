let context = new AudioContext()

const frequencies = {
    a:261.63,
    w:277.18,
    s:293.66,
    e:311.13,
    d:329.63,
    f:349.23,
    t:369.99,
    g:392.00,
    y:415.30,
    h:440.00,
    u:466.16,
    j:493.88,

}

function playNote(event){
    if(!frequencies[event.key]) return
    console.log("key pressed", event.key)
    document.getElementById(event.key).classList.add('white-key-pressed')
    const oscilator = context.createOscillator()
    const gainNode = context.createGain()
    oscilator.type="triangle"
    gainNode.gain.exponentialRampToValueAtTime(0.0001, context.currentTime + 0.3)
    oscilator.frequency.value = frequencies[event.key]
    oscilator.connect(gainNode)
    oscilator.connect(context.destination)
    oscilator.start()
    oscilator.stop(context.currentTime+ 0.3)

}

function clearNote(event){
    if(!frequencies[event.key]) return
    document.getElementById(event.key).classList.remove('white-key-pressed')

}
